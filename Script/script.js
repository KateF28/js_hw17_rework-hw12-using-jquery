// ##Задание

// Переделать домашнее задание №12 используя jQuery

let $scriptTag = $('script')[0];
const $fragment = $(document.createDocumentFragment());

$("#drawCircleBtn").click((event) => {
    $(event.currentTarget).css({ "display": 'none' });

    $fragment.append(
    `<p class="circleDataText">Enter circle diameter in millimeters. It should be a positive integer:</p>
     <input class="circleData" type="number" placeholder="20 for example"/>`);

    let $circleColorInputText = $('<p/>');
    $circleColorInputText.attr({
        class: "circleDataText",
    });
    $circleColorInputText.text("Enter a color of a circle in 'color', RGB, HEX or HSL format:");
    $fragment.append($circleColorInputText);
    let $circleColorInput = $('<input/>', {
        class: 'circleData',
        type: 'text',
        placeholder: "hsl(132, 56%, 30%) for example",
    });
    $fragment.append($circleColorInput);

    let $drawBtn = $('<button/>');
    $drawBtn.addClass('drawCircleBtn')
    $drawBtn.text('TO DRAW');
    $fragment.append($drawBtn);

    $fragment.insertBefore($scriptTag);

    $(".drawCircleBtn").click(() => {
        let $circle = $('<div/>', {
        class: 'circle',
    });
        // $circle.css({ "width": '20mm', "height": '20mm', "background-color": 'red'});
        $circle.css({ "width": `${$("input[type=number]").val()}mm`, "height": `${$("input[type=number]").val()}mm`, "background-color": `${$circleColorInput.val()}`});

        $scriptTag.before($circle);
        // $circle.insertBefore($scriptTag);
    });

});